import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SalesReportChartComponent } from './sales-report-chart/sales-report-chart.component';
import {ChartsModule} from 'ng2-charts';
import {FlightService} from './sales-report-chart/flight.service';
import {HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    AppComponent,
    SalesReportChartComponent
  ],
    imports: [
        BrowserModule,
      NgxSpinnerModule,
        AppRoutingModule,
        ChartsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatButtonToggleModule
    ],
  providers: [
    FlightService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
