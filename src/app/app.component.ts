import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public spinnerConfig: any;
  title = 'test';
  constructor() {
    this.spinnerConfig = {type: 'ball-clip-rotate', color: '#ccc', text: 'در حال دریافت اطلاعات'};
  }
}
