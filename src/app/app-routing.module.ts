import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SalesReportChartComponent} from './sales-report-chart/sales-report-chart.component';


const routes: Routes = [
  {path: '', component: SalesReportChartComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
