import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FlightService {

  constructor(private httpClient: HttpClient) { }

  getFlightsReport(time, status) {
    return this.httpClient.get<any>(`test/sales.${time}.${status}.json`);
  }

}
