import { Component, OnInit } from '@angular/core';
import {ChartDataSets} from 'chart.js';
import {Color, Label} from 'ng2-charts';
import {FlightService} from './flight.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-sales-report-chart',
  templateUrl: './sales-report-chart.component.html',
  styleUrls: ['./sales-report-chart.component.scss']
})
export class SalesReportChartComponent implements OnInit {

  lineChartData: ChartDataSets[] = [];

  lineChartLabels: Label[];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'rgb(255, 142, 170)',
      backgroundColor: 'rgba(255, 142, 170, 0.2)',
    },
    {
      borderColor: 'rgb(166, 172, 255)',
      backgroundColor: 'rgba(166, 172, 255, 0.2)',
    }
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';
  private sale = [];
  private saleDate = [];
  private cancelled = [];
  private endCall = false;

  constructor(private flightService: FlightService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getReport('month');
  }

  getReport(time) {
    this.spinner.show();
    this.flightService.getFlightsReport(time, 'SUCCESSFUL').subscribe( successData => {
      if (this.endCall) {
        this.setData();
        this.spinner.hide();
      }
      this.endCall = true;
      this.sale = [];
      this.saleDate = [];
      for (const item of successData) {
        this.sale.push(item.amount);
        const date = new Date(item.date * 1000).toLocaleDateString('fa-IR');
        this.saleDate.push(date);
      }
      this.lineChartLabels = this.saleDate;
    });
    this.flightService.getFlightsReport(time, 'CANCELED').subscribe( cancelData => {
      if (this.endCall) {
        this.setData();
        this.spinner.hide();
      }
      this.endCall = true;
      this.cancelled = [];
      for (const item of cancelData) {
        this.cancelled.push(item.amount);
      }
    });
  }
  setData() {
    setTimeout( () => {
      this.lineChartData = [
        { data: this.cancelled, label: 'کنسلی' },
        { data: this.sale, label: ' فروش' }
      ];
    }, 0);
  }
}
